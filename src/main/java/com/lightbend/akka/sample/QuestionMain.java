package com.lightbend.akka.sample;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.*;

/**
 * Created by Anvarbek Kuvandikov on 13.02.2020 9:29
 */
public class QuestionMain extends AbstractBehavior<QuestionMain.Say> {

    public static class Say {
        public final String name;
        public final String question;

        public Say(String name, String question) {
            this.name = name;
            this.question = question;
        }
    }

    private final ActorRef<User.Question> greeter;

    public static Behavior<Say> create() {
        return Behaviors.setup(QuestionMain::new);
    }

    private QuestionMain(ActorContext<Say> context) {
        super(context);
        //#create-actors
        greeter = context.spawn(User.create(), "Bot");
        //#create-actors
    }

    @Override
    public Receive<Say> createReceive() {
        return newReceiveBuilder().onMessage(Say.class, this::onSay).build();
    }

    private Behavior<Say> onSay(Say command) {
        //#create-actors

//        System.out.println("#onSay:"+command.name+","+command.question);
        ActorRef<User.AskedQuestion> replyTo =
                getContext().spawn(Bot.create(1), command.name);
        greeter.tell(new User.Question(command.name, command.question, replyTo));
        //#create-actors
        return this;
    }
}
