package com.lightbend.akka.sample;

import akka.actor.typed.ActorSystem;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class AkkaQuickstart {
  public static void main(String[] args) {
//    System.setProperty("java.net.useSystemProxies", "true");
    final ActorSystem<QuestionMain.Say> greeterMain = ActorSystem.create(QuestionMain.create(), "helloakka");
    //#actor-system

    Scanner sr=new Scanner(System.in);
    //#main-send-messages
    System.out.println(">>> Please, ask your question <<<");
    String isWork="N";
    do {
      System.out.print("User: ");
      String question=sr.nextLine();
      greeterMain.tell(new QuestionMain.Say( "User",question));
      isWork=sr.nextLine();
    }while (isWork.toUpperCase().equals("Y"));
    //#main-send-messages
    greeterMain.terminate();

  }
}
