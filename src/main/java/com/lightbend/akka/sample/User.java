package com.lightbend.akka.sample;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.*;
import java.util.Objects;

/**
 * Created by Anvarbek Kuvandikov on 12.02.2020 15:57
 */
public class User extends AbstractBehavior<User.Question> {

    public static final class Question {
        public final String whom;
        public final String question;
        public final ActorRef<AskedQuestion> replyTo;

        public Question(String whom, String question, ActorRef<AskedQuestion> replyTo) {
            this.whom = whom;
            this.question = question;
            this.replyTo = replyTo;
        }
    }

    public static final class AskedQuestion {
        public final String whom;
        public final String answer;
        public final ActorRef<Question> from;

        public AskedQuestion(String whom, String answer, ActorRef<Question> from) {
            this.whom = whom;
            this.answer = answer;
            this.from = from;
        }

        // #greeter
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AskedQuestion greeted = (AskedQuestion) o;
            return Objects.equals(whom, greeted.whom) &&
                    Objects.equals(from, greeted.from);
        }

        @Override
        public int hashCode() {
            return Objects.hash(whom, from);
        }

        @Override
        public String toString() {
            return "AskedQuestion{" +
                    "whom='" + whom + '\'' +
                    ", from=" + from +
                    '}';
        }
// #greeter
    }

    public static Behavior<Question> create() {
        return Behaviors.setup(User::new);
    }

    private User(ActorContext<Question> context) {
        super(context);
    }

    @Override
    public Receive<Question> createReceive() {
        return newReceiveBuilder().onMessage(Question.class, this::onQuestion).build();
    }

    private Behavior<Question> onQuestion(Question command) {
//        getContext().getLog().info("Hello {}! {}", command.whom,command.question);
        //#greeter-send-message

        command.replyTo.tell(new AskedQuestion(command.whom, "answer", getContext().getSelf()));
        //#greeter-send-message
        return this;
    }
}

