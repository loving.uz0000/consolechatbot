package com.lightbend.akka.sample;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Anvarbek Kuvandikov on 13.02.2020 10:41
 */
public class HTTPRequest {
    private CloseableHttpClient client;
    private HttpPost httpPost;
    public HTTPRequest() {
         client= HttpClients.createDefault();
         httpPost=new HttpPost("https://odqa.demos.ivoice.online/model");
    }
    public String getAnswer(String question){
        JSONArray array=new JSONArray();
        array.put(question);
        JSONObject object=new JSONObject();
        object.put("context",array);
//        System.out.println(">>> Please, wait <<<");
        StringEntity entity=new StringEntity(object.toString(),"UTF-8");
        entity.setContentType("application/json");
        httpPost.setEntity(entity);
        try (CloseableHttpResponse response = client.execute(httpPost)){
            int result=response.getStatusLine().getStatusCode();
            if (result==200){
                String jsonString = EntityUtils.toString(response.getEntity(), "UTF-8");
                JSONArray jsonArray = new JSONArray(jsonString);
                String ans=((JSONArray) ((JSONArray)jsonArray.get(0)).get(0)).get(0).toString();
                return ans;
            }
            else {
                return "Error";
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
//            System.out.println("JA");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "No connection to the server";
    }
}
