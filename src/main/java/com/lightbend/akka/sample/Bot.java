package com.lightbend.akka.sample;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.*;

/**
 * Created by Anvarbek Kuvandikov on 12.02.2020 16:11
 */
public class Bot extends AbstractBehavior<User.AskedQuestion> {

    public static Behavior<User.AskedQuestion> create(int max) {
        return Behaviors.setup(context -> new Bot(context, max));
    }

    private final int max;
    private int greetingCounter;

    private Bot(ActorContext<User.AskedQuestion> context, int max) {
        super(context);
        this.max = max;
    }

    @Override
    public Receive<User.AskedQuestion> createReceive() {
        return newReceiveBuilder().onMessage(User.AskedQuestion.class, this::onAskedQuestion).build();
    }

    private Behavior<User.AskedQuestion> onAskedQuestion(User.AskedQuestion message) {
        greetingCounter++;

       HTTPRequest httpRequest=new HTTPRequest();
       String answer=httpRequest.getAnswer("can i work and study at the same time?");
        System.out.println("Bot: "+answer);
        System.out.println(">>> Ask next question? Y/n <<<");
//        getContext().getLog().info("Greeting {} for {}", greetingCounter, message.whom);
        if (greetingCounter == max) {
            return Behaviors.stopped();
        } else {
            message.from.tell(new User.Question(message.whom, message.answer, getContext().getSelf()));
            return this;
        }
    }
}